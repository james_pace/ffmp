from setuptools import setup

setup(name='ffmp',
      version='0.0.0',
      description='For Fun Motion planner.',
      url='',
      author='James Pace',
      author_email='jpace121@gmail.com',
      license='Apache 2.0',
      packages=['ffmp', 'ffmp.algorithms'],
      install_requires=[],
      zip_safe=False)
