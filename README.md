# For Fun Motion Planner (ffmp) #

A lattice based motion planner written for fun. 

## History ##

My first exposure to lattice based motion planning was with the SBMPO 
algorithm at the FAMU-FSU College of Engineering. My experience 
with SBMPO (and later work with other MPC methods) showed me that lattice
based motion planning is an extremely powerful tool that can be used in 
a number of important applications. 

In late 2017 I was exposed to the SBPL library (developed at Carnegie
Meelon University), with has very similar goals to FFMP. It is my
opinion that due to my previous work with SBMPO, FFMP makes
relatively minor, but significant improvements to the API exposed
by SBPL.

## What's the goal? ##

SBMPO is a rather involved algorithm, but with rather simple underpinnings.
When working with the algorithm, my current strategy (and the only one that
I've seen) is to think about the algorithm in a simplified form and then
apply a series of memorized exceptions to my mental model as I go. This
implementation is a simplified implementation of SBMPO that more closely
aligns with my mental model.

FFMP also aims to be more easily extensible than versions I've seen
of SBMPO.

## Why Python? ##

My long term goal is to also make a C++ implementation once I have the structure
down and have made decisions about what simplifications to the algorithm I want
to make.

I think a Python version will also be easier to use with the gazillion and one
Python Machine Learning frameworks.

I'm not writing any more MATLAB, stop asking.

Examples can be found in the ```examples``` subdirectory below and 
require matplotlib to be installed.

To run examples use commands:
``` 
pip3 install --user -r requirements.txt
pip3 install --user . -e .
python3 examples/*.py
```

```requirements.txt``` contains the packages in the Anaconda environment
I'm currently testing in.

## License ##

FFMP is licensed under the Apache 2.0 License. If you would like to license
it under different terms, please reach out.
