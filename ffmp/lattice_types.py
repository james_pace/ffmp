#  Copyright 2017-2018 James Pace
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
Common classes used for sbmpo-type graph search algorithms.

These classes should be extended by the user for their aplication.
"""

class Interface:
    """
    Base class for an interface between the planner and the system.
    Defines the minimum neccessary methods neccessary for the rest of
    the planner.
    """
    def command(self, nodes):
        """
        Given the best nodes chosen by the planner, do something. 
        """
        raise NotImplementedError("Must define an external interface!")

    def current_state(self):
        """
        Return the current state of the system.
        """
        raise NotImplementedError("Must define update_state!")

class State:
    """
    Base class for planner state, defining minimum methods that
    must be implemented.
    """
        
    def is_valid(self, constraint):
        """
        Checks if state is valid, given the constraints on the state.
        Returns true if the state valid, false if it is not valid.
        """
        return True

    def integrate_model(self, control):
        """
        Given a control, calculate the next state.
        """
        raise NotImplementedError("Must define model!")

    def cost(self, past_state):
        """
        Calculates the cost between the current and previous state (i.e. \delta g)
        """
        raise NotImplementedError("Must define cost function!")
    
    def heuristic(self, goal_state):
        """
        Calculates the heuristic, i.e. the estimates cost to goal, from the current state
        """
        return 0

    def grid_point(self):
        """
        Calculate a hash value representing the states grid location.
        To turn off grid, have return a unique number regardless of states,
        i.e. id(self).
        """
        return id(self)

    def at_goal(self, goal):
        """
        Used to calculate if the current state is at the goal or not.
        """
        raise NotImplementedError("Must define function to tell if at goal!")

    def at_horizon(self):
        """
        Used to calculate if we are at the end of the receeding horizon
        or not.
        To not do receeding horizon, always return False.
        """
        return False
    
class Control:
    """
    Base state for the input to the model being integrated.

    Should correspond with the input expected by the implementation
    of integrate_model in the State class.

    """
    def __init__(self):
        """
        Constructor.

        Values used as controls by integrate model should be filled in here.
        """
        raise NotImplementedError("Constructor for Control is missing.")

    def is_valid(self, constraint):
        """
        Determine if this control is valid, given some constraint, that may
        may change with time.
        """
        is_valid = True # Assuming using fixed, all valid, samples right now.
        return is_valid

class Grid:
    def __init__(self):
        self._dict = {}

    def is_occupied(self, node):
        """
        Determines if state is in grid using the state's grid_point method.
        Returns a tuple containing if the grid cell is occupied, and if it is,
        the node in that grid cell.
        """
        if node.state.grid_point() in self._dict.keys():
            return (True, self._dict[node.state.grid_point()])
        else:
            return (False, None)

    def put(self, node):
        """ 
        Insert node into the grid. If grid cell is already occupied, this
        function overides the node at the grid cell.
        """
        self._dict[node.state.grid_point()] = node
