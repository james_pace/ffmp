#  Copyright 2017-2018 James Pace
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
A* implementation of SBMPO.
"""
import math
from queue import PriorityQueue

def run(init_state, goal_state, samples, interface, control_constraints, state_constraints, heur_weight, Grid):
   """
   Given start and goal states, the samples for the control, and the states run SBMPO
   to generate an optimal trajectory
   """
   init_node = Node(init_state,
                    0, # prev_node is null
                    0, # there is no control to get to prev_node
                    0, # there is no cost to here
                    init_state.heuristic(goal_state), # there is a cost-to-goal
                    heur_weight
                    )
   queue = PriorityQueue(0) # no max size
   grid = Grid()

   cur_node = init_node
   horizon_init_state = init_state
   queue.put(init_node)
   grid.put(init_node)
   
   while True:
       if cur_node.state.at_goal(goal_state):
           # Made it to the goal, so break;
           break;
       if cur_node.state.at_horizon():
           # Send nodes to interface.
           full_path = []
           inc_node  = cur_node
           while inc_node.state != horizon_init_state:
               full_path.append(inc_node)
               inc_node = inc_node.prev_node
           full_path.append(inc_node) # to grab the initial node
           # send to robot/exceute/add to list
           # assuming that the interface enforces the control horizon
           full_path.reverse()
           interface.command(full_path)
           # reset queue
           queue = PriorityQueue(0);
           # get current next state from plant
           # assume that the interface enforces that control
           # horizon number of controls should have been ran before
           # it returns anything
           new_init_state = interface.current_state()
           horizon_init_state = new_init_state # update initial state for horizon
           # reset initial state to be new state
           new_init_node = Node(new_init_state,
                            0, # prev_node is null
                            0, # there is no control to get to prev_node
                            0, # there is no cost to here
                            new_init_state.heuristic(goal_state), # there is a cost-to-goal
                            heur_weight
                        )
           queue.put(new_init_node)
       if not queue.empty():
           cur_node = queue.get_nowait()
           generate_children(cur_node, goal_state, queue, grid, samples,
                             control_constraints, state_constraints, heur_weight)
       else:
           print("Queue is empty.")
           break

   if cur_node.state.at_goal(goal_state): # aka broke out of while loop for that reason.
        # Save last list of nodes.
        best_nodes = []
        inc_node = cur_node

        while inc_node.state != horizon_init_state:
            best_nodes.append(inc_node)
            inc_node = inc_node.prev_node

        best_nodes.append(inc_node) # to grab the initial node
   else:
        best_nodes = []
        print("Did not reach goal.")

   best_nodes.reverse()

   interface.command(best_nodes)
        
class Node:
    """
    Representation of node in tree.
    """
    def __init__(self, state, prev_node, control, g, h, weight):
        self.state = state
        self.prev_node = prev_node
        self.control = control
        self.g = g
        self.h = h
        self.f = g + weight*h
        
    # Comparison operators for sorting.
    def __eq__(self, other):
        return self.f == other.f
    def __lt__(self, other):
        return self.f < other.f
    def __le__(self, other):
        return self.f <= other.f
    def __ne__(self, other):
        return self.f != other.f
    def __gt__(self, other):
        return self.f > other.f
    def __ge__(self, other):
        return self.f >= other.f

def generate_children(prev_node, goal_state, priority_queue, implicit_grid, samples, control_constraints, state_constraints, heur_weight):
    """
    Given a node and the priority queue, generate children for said node and
    insert them into the priority queue.

    Also need to know possible samples and constraints on controls and states.

    I'm not sure I like modifying the queue in here, but I think this may parrallelize better.
    """
    for control in samples:
        if control.is_valid(control_constraints):
            next_state = prev_node.state.integrate_model(control)
            if next_state.is_valid(state_constraints):
                next_g = prev_node.g + next_state.cost(prev_node.state)
                next_h = next_state.heuristic(goal_state)
                new_node = Node(next_state, prev_node, control, next_g, next_h, heur_weight)

                (grid_occupied, node_in_grid) = implicit_grid.is_occupied(new_node)

                if grid_occupied: # so state is already in grid
                    if new_node.g <= node_in_grid.g: # if current node is cheaper
                        # put new node in grid replacing last node and also
                        # insert into the queue
                        priority_queue.put(new_node)
                        implicit_grid.put(new_node)
                    else: # new node is more expensive then node already found
                        pass; # do nothing
                else: # if grid cell not occupied, put node in grid and queue
                    priority_queue.put(new_node)
                    implicit_grid.put(new_node)


