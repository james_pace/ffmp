FROM ubuntu:18.04

RUN apt-get update && apt-get install -y \
  python3-pip 
RUN pip3 install ipython

COPY requirements.txt .
RUN pip3 install -r requirements.txt

COPY . ./ffmp
WORKDIR ./ffmp

RUN pip3 install .

