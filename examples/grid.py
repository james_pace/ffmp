#  Copyright 2017-2018 James Pace
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import matplotlib.pyplot as plt
import math
import ffmp.lattice_types as lattice_types
import ffmp.algorithms.a_star as a_star

class GridWorld_Interface(lattice_types.Interface):
    def __init__(self, init_state):
        self.cur_state = init_state
        
    def command(self, nodes):
        state_x = []
        state_y = []
        for node in nodes:
            state_x.append(node.state.x)
            state_y.append(node.state.y)

        # Update cur_state
        self.cur_state = GridWorld_State(state_x[-1], state_y[-1])

        fig = plt.Figure()
        plt.axis('equal')
        cur_ax = plt.gca()
        cur_ax.plot(state_x, state_y, 'ro-')
        cur_ax.grid(linestyle='--')

        plt.show()
        
    def current_state(self):
        return self.cur_state
        

class GridWorld_Control(lattice_types.Control):
    def __init__(self, val):
        self.val = val
        
    def is_valid(self, constraint):
        is_valid = True # Assuming using fixed, all valid, samples right now.
        return is_valid

class GridWorld_State(lattice_types.State):
    def __init__(self, x, y):
        self.x = x
        self.y = y
    def is_valid(self, constraint):
        """
        Checks if state is valid, given a dictionary with the constraints
        on the states.
        """
        is_valid = True
        if self.x > constraint["max_x"]:
            is_valid = False
        if self.x < constraint["min_x"]:
            is_valid = False
        if self.y > constraint["max_y"]:
            is_valid = False
        if self.y < constraint["min_y"]:
            is_valid = False
        return is_valid

    def integrate_model(self, control):
        """
        Given a control, calculate the next state.
        """
        if control.val == "f":
            next_state = GridWorld_State(self.x, self.y+1)
        elif control.val == "b":
            next_state = GridWorld_State(self.x, self.y-1)
        elif control.val == "l":
            next_state = GridWorld_State(self.x-1, self.y)
        elif control.val == "r":
            next_state = GridWorld_State(self.x+1, self.y)
        return next_state

    def cost(self, past_state):
        """
        Calculates the cost between the current and previous state (i.e. \delta g)
        """
        return math.sqrt( (self.x - past_state.x)**2 + (self.y - past_state.y)**2)
    
    def heuristic(self, goal_state):
        """
        Calculates the heuristic, i.e. the estimates cost to goal, from the current state
        """
        return math.sqrt( (self.x - goal_state.x)**2 + (self.y - goal_state.y)**2)

    def grid_point(self):
        """
        Calculate a hash value representing the states grid location.
        To turn off grid, have return a unique number regardless of states,
        i.e. id(self).
        """
        grid_res_x = 1
        grid_res_y = 1

        return (53*math.floor(self.x/grid_res_x) + 97*math.floor(self.y/grid_res_y))

    def at_goal(self, goal):
        x_offset = math.fabs(self.x - goal.x)
        y_offset = math.fabs(self.y - goal.y)
        
        return math.sqrt(x_offset**2 + y_offset**2) < 0.5 # 0.5 is goal thresh

def main():
    init_state = GridWorld_State(1,1)
    goal_state = GridWorld_State(8,2)
    samples = [GridWorld_Control("f"), GridWorld_Control("b"), GridWorld_Control("l"), GridWorld_Control("r")]
    control_constraints = {}
    state_constraints = {"min_x": 0, "max_x": 10, "min_y": 0, "max_y":10}

    interface = GridWorld_Interface(init_state)
    
    a_star.run(init_state, goal_state, samples, interface, control_constraints, state_constraints, lattice_types.Grid)
                         
if __name__ == "__main__":
    main()


