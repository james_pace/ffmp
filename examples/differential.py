#  Copyright 2017-2018 James Pace
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import matplotlib.pyplot as plt
import math
import numpy as np
import ffmp.lattice_types as lattice_types
import ffmp.algorithms.a_star as a_star

class Differential_Interface(lattice_types.Interface):
    def __init__(self, start_state):
        self.cur_state = start_state

    def command(self, nodes):
        print("Plotting")
        
        state_x = []
        state_y = []
        for node in nodes:
            state_x.append(node.state.x)
            state_y.append(node.state.y)

        last_x = nodes[-1].state.x
        last_y = nodes[-1].state.y
        last_theta = nodes[-1].state.theta
        self.cur_state = Differential_State(last_x, last_y, last_theta)

        fig = plt.Figure()
        plt.axis('equal')
        cur_ax = plt.gca()
        cur_ax.plot(state_x, state_y, 'ro-')
        cur_ax.grid(linestyle='--')

        plt.show()

    def current_state(self):
        return self.cur_state

class Differential_Control(lattice_types.Control):
    def __init__(self, vel, ang_vel):
        self.vel = vel # vehicle forward velocity
        self.ang_vel = ang_vel # body angular velocity
        
    def is_valid(self, constraint):
        is_valid = True # Assuming using fixed, valid, samples right now.
        return is_valid

class Differential_State(lattice_types.State):
    def __init__(self, x, y, theta):
        self.x = x
        self.y = y
        self.theta = theta
    def is_valid(self, constraint):
        """
        Need to check if within bounds and if near obstacle.

        constraint:
            min_x
            min_y
            obstacles[]: (x,y,r)
        """
        is_valid = True

        # Boundary check
        if self.x > constraint["max_x"] or self.x < constraint["min_x"]:
            is_valid = False
        if self.y > constraint["max_y"] or self.y < constraint["min_y"]:
            is_valid = False

        # obstacle check, assuming round obstacles
        for obstacle in constraint["obstacles"]:
            obs_dist = math.sqrt((self.x - obstacle(0))**2 + (self.y - obstacle(1))**2)
            if obs_dist <= obstacle(2):
                is_valid = False
        
        return is_valid

    def integrate_model(self, control):
        """
        Given a control, calculate the next state.
        """
        # Robot body measurements (using ~TRTZ values)
        # TODO: Pass in constructor.
        wheel_rad = 0.039/2 #m #only needed to find wheel vels
        body_width = 0.098 #m #onlue needed to find wheel vels
        time_step = 0.5
        # 1. Rotate into global frame
        V_x = control.vel*math.cos(self.theta)
        V_y = control.vel*math.sin(self.theta)
        # 2. Integrate
        new_x = self.x + V_x*time_step
        new_y = self.y + V_y*time_step
        new_theta  = self.theta + control.ang_vel*time_step
        # 3. Build new state
        next_state = Differential_State(new_x, new_y, new_theta)
        
        return next_state

    def cost(self, past_state):
        """
        Calculates the cost between the current and previous state (i.e. \delta g)
        """
        return math.sqrt( (self.x - past_state.x)**2 + (self.y - past_state.y)**2)
    
    def heuristic(self, goal_state):
        """
        Calculates the heuristic, i.e. the estimates cost to goal, from the current state
        """
        return math.sqrt( (self.x - goal_state.x)**2 + (self.y - goal_state.y)**2)

    def grid_point(self):
        """
        Calculate a hash value representing the states grid location.
        To turn off grid, have return a unique number regardless of states,
        i.e. id(self).
        """
        grid_res_x = 0.025
        grid_res_y = 0.025
        
        #return (53*math.floor(self.x/grid_res_x) + 97*math.floor(self.y/grid_res_y))
        return id(self)

    def at_goal(self, goal):
        x_offset = math.fabs(self.x - goal.x)
        y_offset = math.fabs(self.y - goal.y)
        
        return math.sqrt(x_offset**2 + y_offset**2) < 0.25 # 0.05 is goal thresh

def build_samples():
    vel_dist = np.array([0.5])
    ang_vel_dist = np.random.normal(0, 3.14/6, 10)

    samples = []
    for vel_opt in vel_dist:
        for ang_vel_opt in ang_vel_dist:
            samples.append(Differential_Control(vel_opt, ang_vel_opt))

    return samples
            
    

def main():
    init_state = Differential_State(0, 0, 0.0)
    goal_state = Differential_State(0, 2, 0.0)
    samples = build_samples()
    control_constraints = {}
    state_constraints = {"min_x": -1, "max_x": 4, "min_y": -1, "max_y":4, "obstacles": ()}
    interface = Differential_Interface(init_state)
    
    a_star.run(init_state, goal_state, samples, interface, control_constraints, state_constraints, lattice_types.Grid)
    
if __name__ == "__main__":
    main()


